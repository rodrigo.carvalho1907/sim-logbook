from flask import Flask, redirect, url_for, render_template, request, session, flash
from datetime import timedelta
from flask_pymongo import PyMongo
from settings import config_for
import os

app = Flask(__name__)
app.secret_key = 'hello'
app.permanent_session_lifetime = timedelta(minutes=5)
config_name = os.environ.get('APP_CONFIG', 'development')
config = config_for(f'config.{config_name}')
app.config.from_object(config)
app.db = PyMongo(app).db
db = app.db['flights']

@app.route('/home')
@app.route('/')
def home():
    return render_template('home.html')

@app.route('/privacy')
def privacy():
    return render_template('privacy.html')

@app.route('/flights')
def flights():
    results = db.find()
    return render_template('flights.html', _flights=results)

@app.route('/newflight', methods=['GET', 'POST'])
def newflight():
    if request.method == 'POST':
        data = request.form
        flight = {
            'number': data['number'],
            'aircraft': data['aircraft'],
            'departure': data['departure'],
            'arrival': data['arrival']
        }
        db.insert_one(flight)
        return 'Flight Registered ! <a href="/flights">Continue</a>'
    return render_template('newflight.html')
