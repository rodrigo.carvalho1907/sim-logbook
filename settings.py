import logging
from os import environ
from dotenv import load_dotenv, find_dotenv

load_dotenv()


class _Config:
    MONGO_URI = environ.get('MONGO_URI', 'mongodb://localhost:27017/flights')
    
class _DevelopmentConfig(_Config):
    logging.basicConfig(level=logging.DEBUG)
    DEBUG = True

class _ProductionConfig(_Config):
    logging.basicConfig(level=logging.INFO)

_CONFIG = {
    'production': _ProductionConfig,
    'development': _DevelopmentConfig
}

def config_for(name):
    if name in _CONFIG:
        return _CONFIG[name]

    logging.warn(f'using default configuration:attempted to use invalid configuration "{name}"')

    return _Config
